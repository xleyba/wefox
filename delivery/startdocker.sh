#!/bin/bash

## ---------------------------------------------------
## Script to start wefox app in local environment
##  accept a string as parameter for mvn 
## ie:  bash startlocal.sh "-s /Users/jlr/Downloads/settingscosmejav.xml"
## ---------------------------------------------------

# Export section
export g="\033[32m"       #GREEN
export n="\033[0m"        #NORMAL
export yb="\033[1;33m"    #YELLOWBOLD
export rb="\033[1;31m"    #REDBOLD

valid=true

# Build application
cd ../wefoxapp
mvn clean package $1

# Copy jar file to bin
rm ../delivery/wefoxapp/bin/wefoxapp-0.0.1.jar
cp ./target/wefoxapp-0.0.1.jar ../delivery/wefoxapp/bin/.


# Run docker compose
cd ../delivery
docker-compose -f compose-wefox.yml up -d

# Start producer app
curl http://localhost:9000/start

