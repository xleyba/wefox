#!/bin/bash

## ---------------------------------------------------
## Script to start wefox app in local environment
##  accept a string as parameter for mvn 
## ie:  bash startlocal.sh "-s /Users/jlr/Downloads/settingscosmejav.xml"
## ---------------------------------------------------

# Export section
export g="\033[32m"       #GREEN
export n="\033[0m"        #NORMAL
export yb="\033[1;33m"    #YELLOWBOLD
export rb="\033[1;31m"    #REDBOLD

valid=true

# Build application
cd ../wefoxapp
mvn clean package $1


# Run docker compose
cd ../delivery
docker-compose up -d

# Check if application started
while [ $valid ]
do
if [ -z `docker-compose ps -q api-producer` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q api-producer)` ]; then
  echo "Waiting for docker"
else
  break
fi
done

# Start producer app
curl http://localhost:9000/start

# Start app
cd ../wefoxapp
mvn spring-boot:run $1

# Stop docker compose
cd ../delivery
docker-compose down
