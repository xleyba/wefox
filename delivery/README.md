# Challenge

## :computer: How to execute

### Local environment

Application configurations is prepared to run in local environment by default.

Tu build and run the application, go to delivery folder and run the script as follows:

```bash
./startlocal.sh
```

If you need to pass parameters to the script for maven ie: the settings file) you can do it like this:

```bash
./startlocal.sh "-s /Users/jlr/Downloads/settings.xml"
```

To stop the process press Ctrl-C

### Docker environment

To run the application inside a docker compose, go to delivery folder and run this:

```bash
./startdocker.sh
```

## :memo: Notes

- I am not working as developer except for some PoC that I use to do, however accepted the challenge.
- I had no time to create all the tests but added just one.
- The application was created as microservice (kind of). 

## :pushpin: Things to improve

- even when the delivered microservice application could escalate, I think that it would need some improvements (a controller, the use of actuator, a framework to register with a service registry like Consul, etc)
- check how ConcurrentKafkaListenerContainerFactory handle threads and whether the multithreading 
could be enhanced to get a better perfomance.
- Perhaps split online and offline processing in two different micros for better paralell processing.
- I observed that log service fail some times. As the idea is do not lose any data, perhaps a new topic could be created to drop the error messages there and then send them to the service (even change the service to be a kafka consumer for such topic) or use ELK (or tools like that) to collect the data.
