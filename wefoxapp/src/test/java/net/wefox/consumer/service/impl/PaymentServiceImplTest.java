package net.wefox.consumer.service.impl;

import net.wefox.consumer.dto.PaymentRepository;
import net.wefox.consumer.payment.domain.Payment;
import net.wefox.consumer.service.PaymentService;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.test.util.AssertionErrors;

import java.math.BigDecimal;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/*
 * Test Payment Service
 */
public class PaymentServiceImplTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @BeforeEach
    public void setUp() {

    }

    /*
     * Test success case for save operation.
     */
    @Test
    public void savePayment_success() {
        // Prepare Repository and Service
        PaymentRepository paymentRepository = Mockito.mock(PaymentRepository.class);
        PaymentService paymentService = new PaymentServiceImpl(paymentRepository);

        // Create Payment Object
        Payment p = new Payment();
        p.setAccountId(1);
        p.setAmount(new BigDecimal(1));
        p.setPaymentId("1");
        p.setCreditCard("1");
        p.setPaymentType("online");

        when(paymentRepository.save(ArgumentMatchers.any(Payment.class))).thenReturn(p);

        Payment newPayment = paymentService.savePayment(p);

        AssertionErrors.assertEquals("Verify Payment", newPayment, p);
        verify(paymentRepository).save(p);

    }
}
