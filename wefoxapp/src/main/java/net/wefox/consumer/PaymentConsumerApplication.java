package net.wefox.consumer;


import lombok.extern.slf4j.Slf4j;
import net.wefox.consumer.payment.domain.Payment;
import net.wefox.consumer.service.AccountService;
import net.wefox.consumer.service.PaymentErrorService;
import net.wefox.consumer.service.PaymentService;
import net.wefox.consumer.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.web.client.RestClientException;

import java.sql.Timestamp;

/*
 * Payment consumer application definition.
 * Will consume Payments from Kafka topics (online, offline), validate online payments,
 * save them to DB and update account with last_payment timestamp.
 * Errors will be sent to log service.
 */
@SpringBootApplication
@Configuration
@Slf4j
public class PaymentConsumerApplication implements CommandLineRunner {

    @Autowired
    private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private AccountService accoutnService;

    @Autowired
    private PaymentErrorService paymentErrorService;

    private boolean isPaymentValid;


    public static void main(String[] args) {
        SpringApplication.run(PaymentConsumerApplication.class, args);
    }

    /*
     * Run the application by starting the kafka listener
     */
    @Override
    public void run(String... args) {
        MessageListenerContainer listenerContainer = kafkaListenerEndpointRegistry.getListenerContainer("consumerapp");
        listenerContainer.start();

        Runtime.getRuntime().addShutdownHook(new Thread(listenerContainer::stop));
    }

    /*
     * Kafka listener that will consume topics.
     */
    @KafkaListener(id = "consumerapp", topics = "#{'${spring.kafka.topics}'.split(',')}",
            containerFactory = "paymentKafkaListenerContainerFactory",
            autoStartup = "false")
    private void paymentListener(Payment payment) {

        log.info("Received - payment ID {} - account ID {}", payment.getPaymentId(), payment.getAccountId());

        try {
            isPaymentValid = true;

            if ("online".equals(payment.getPaymentType())) {
                isPaymentValid = validationService.validatePayment(payment);
            }

            if (isPaymentValid) {
                Timestamp ts = new Timestamp(System.currentTimeMillis());

                // Save Payment with now timestamp.
                payment.setCreatedOn(ts);
                paymentService.savePayment(payment);
                accoutnService.updateAccountLastPayment(payment);

            }
        } catch (RestClientException rce) {
            log.error("Error: Rest client exception while validating payment ID: {} - {}", payment.getPaymentId(), rce.getMessage());
            paymentErrorService.saveError(payment.getPaymentId(), "network",
                    "Error: Rest client exception while validating payment: " +
                            rce.getMessage());
        } catch (IllegalArgumentException iae) {
            log.error("Error on payment ID {} - DB Illegal Argument Exception: {}", payment.getPaymentId(), iae.toString());
            paymentErrorService.saveError(payment.getPaymentId(), "database",
                    "Error: DB Illegal Argument Exception: " + iae.getMessage());
        } catch (Exception e) {
            log.error("Error on payment ID {} -  Exception {}", payment.getPaymentId(), e.getMessage());
            paymentErrorService.saveError(payment.getPaymentId(), "other",
                    "Error: Exception: " + e.getMessage());
        }

    }

}
