package net.wefox.consumer.service;

import net.wefox.consumer.payment.domain.Payment;

/*
 * Definition of Payment validation service.
 */
public interface ValidationService {

    boolean validatePayment(Payment p);
}
