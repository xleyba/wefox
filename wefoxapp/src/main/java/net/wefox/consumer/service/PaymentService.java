package net.wefox.consumer.service;

import net.wefox.consumer.payment.domain.Payment;

/*
 * Definition of payment service.
 */
public interface PaymentService {
    public Payment savePayment(Payment p);

}
