package net.wefox.consumer.service.impl;


import net.wefox.consumer.payment.domain.Payment;
import net.wefox.consumer.payment.validation.ValidationClient;
import net.wefox.consumer.service.ValidationService;
import org.springframework.stereotype.Service;

/*
 * Definition of Payment validation service.
 */
@Service
public class ValidationServiceImpl implements ValidationService {

    private final ValidationClient validationClient;

    public ValidationServiceImpl(ValidationClient validationClient) {
        this.validationClient = validationClient;
    }

    @Override
    public boolean validatePayment(Payment p) {
        return validationClient.validatePayment(p);
    }
}
