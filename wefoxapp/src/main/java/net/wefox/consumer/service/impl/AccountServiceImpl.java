package net.wefox.consumer.service.impl;

import lombok.extern.slf4j.Slf4j;
import net.wefox.consumer.dto.AccountRepository;
import net.wefox.consumer.payment.domain.Account;
import net.wefox.consumer.payment.domain.Payment;
import net.wefox.consumer.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/*
 + Service layer for account repository operations.
 */
@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAccountLastPayment(Payment p) {
        // WIll check if payment timestamp is really lastpayment to ensure order
        Account account = accountRepository.findById(p.getAccountId()).get();

        // Check if current last_payment_date is newer than the one in current payment
        // to ensure order
        if (account.getLastPaymentDate() == null || (
                account.getLastPaymentDate() != null &&
                        account.getLastPaymentDate().compareTo(p.getCreatedOn()) < 0
        )) {
            account.setLastPaymentDate(p.getCreatedOn());
            accountRepository.save(account);
            log.info("Account id {} saved with new lastPayment {}", account.getAccount_id(),
                    account.getLastPaymentDate());
        } else {
            log.info("Account id: {} not updated", account.getAccount_id());
        }

    }
}
