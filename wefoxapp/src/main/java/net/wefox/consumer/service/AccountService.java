package net.wefox.consumer.service;

import net.wefox.consumer.payment.domain.Payment;

/*
 * Definition of Account service.
 */
public interface AccountService {

    public void updateAccountLastPayment(Payment p);
}
