package net.wefox.consumer.service;

/*
 * Payment error log service definition.
 */
public interface PaymentErrorService {
    public void saveError(String paymentId, String error,
                           String errorDescription);
}
