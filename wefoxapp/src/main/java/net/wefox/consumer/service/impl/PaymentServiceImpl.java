package net.wefox.consumer.service.impl;

import lombok.extern.slf4j.Slf4j;
import net.wefox.consumer.dto.PaymentRepository;
import net.wefox.consumer.payment.domain.Payment;
import net.wefox.consumer.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    public PaymentServiceImpl(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Payment savePayment(Payment p) {
        return paymentRepository.save(p);
    }
}
