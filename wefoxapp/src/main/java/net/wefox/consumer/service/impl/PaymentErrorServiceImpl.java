package net.wefox.consumer.service.impl;

import lombok.extern.slf4j.Slf4j;
import net.wefox.consumer.payment.domain.PaymentError;
import net.wefox.consumer.service.PaymentErrorService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/*
 * Service layer for Payment error log service.
 */
@Slf4j
@Service
public class PaymentErrorServiceImpl implements PaymentErrorService {

    @Value("${error.service.uri}")
    private String errorServiceUrl;

    @Override
    public void saveError(String paymentId, String error, String errorDescription) {

        //ErrorLog errorLog = new ErrorLog(paymentId, error, errorDescription);
        PaymentError paymentError = new PaymentError(paymentId, error, errorDescription);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        HttpEntity<PaymentError> request = new HttpEntity<>(paymentError, headers);

        ResponseEntity<String> result = restTemplate.postForEntity(errorServiceUrl,
                request, String.class);
    }
}
