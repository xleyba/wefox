package net.wefox.consumer.payment.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;
import java.sql.Timestamp;

/*
 + Account entity definition.
 */
@Entity
@Table(name = "accounts")
@Getter
@Setter
@RequiredArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Account {

    @Id
    private Integer account_id;

    private String name;

    private String email;

    private Date birthdate;

    @Column(name = "last_payment_date")
    private Timestamp lastPaymentDate;

    @Column(name = "created_on")
    private Timestamp createdOn;

    @Override
    public String toString() {
        return "Account{" +
                "account_id=" + account_id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", birthdate=" + birthdate +
                ", lastPaymentDate=" + lastPaymentDate +
                ", createdOn=" + createdOn +
                '}';
    }
}