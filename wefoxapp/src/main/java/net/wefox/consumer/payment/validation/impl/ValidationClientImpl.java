package net.wefox.consumer.payment.validation.impl;


import lombok.extern.slf4j.Slf4j;
import net.wefox.consumer.payment.domain.Payment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/*
 * Validation service client.
 * Will call validation rest service to validate online payment.
 */
@Service
@Slf4j
class ValidationClientImpl implements net.wefox.consumer.payment.validation.ValidationClient {

    @Value("${payment.service.uri}")
    private String validationServiceUrl;

    private RestTemplate restTemplate;

    public ValidationClientImpl() {
        this.restTemplate = new RestTemplate();
    }

    public ValidationClientImpl(String validationServiceUrl) {
        this.validationServiceUrl = validationServiceUrl;
        this.restTemplate = new RestTemplate();
    }

    public ValidationClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ValidationClientImpl(String validationServiceUrl, RestTemplate restTemplate) {
        this.validationServiceUrl = validationServiceUrl;
        this.restTemplate = restTemplate;
    }

    @Override
    public boolean validatePayment(Payment p) {

        log.debug("Validating {}", p.getPaymentId());

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        HttpEntity<Payment> request = new HttpEntity<>(p, headers);


        log.debug("Request: {}", request.toString());

        ResponseEntity<String> result = restTemplate.postForEntity(validationServiceUrl,
                request, String.class);


        log.debug("Result was: {}", result);
        log.debug("Result headers {}", result.getStatusCode());


        if (result.getStatusCode() != HttpStatus.OK) {
            return false;
        }

        return true;
    }
}
