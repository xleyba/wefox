package net.wefox.consumer.payment.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;

/*
 * Payment entity definition.
 */
@Entity
@Table(name = "payments")
@Getter
@Setter
@RequiredArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Payment {

    @Id
    @Column(name = "payment_id")
    private String paymentId;       // character varying(100) PRIMARY KEY

    @Column(name = "account_id")
    private Integer accountId;      // integer REFERENCES accounts(account_id) ON DELETE RESTRICT,

    @Column(name = "payment_type")
    private String paymentType;     // character varying(150) NOT NULL,

    @Column(name = "credit_card")
    private String creditCard;      // character varying(100),

    private BigDecimal amount;      // numeric NOT NULL,

    @Column(name = "created_on")
    private Timestamp createdOn;    // timestamp without time zone DEFAULT now()

    @Override
    public String toString() {
        return "Payment{" +
                "paymentId='" + paymentId + '\'' +
                ", accountId=" + accountId +
                ", paymentType='" + paymentType + '\'' +
                ", creditCard='" + creditCard + '\'' +
                ", amount=" + amount +
                ", createdOn=" + createdOn +
                '}';
    }
}
