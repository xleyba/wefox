package net.wefox.consumer.payment.validation;


import net.wefox.consumer.payment.domain.Payment;

/*
 * Validation service for online payment definition.
 */
public interface ValidationClient {

    boolean validatePayment(Payment p);

}
