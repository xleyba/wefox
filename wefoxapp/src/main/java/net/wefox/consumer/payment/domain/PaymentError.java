package net.wefox.consumer.payment.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/*
 * Payment error entity definition
 */
@Getter
@Setter
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PaymentError {

    private String paymentId;

    private String errorType;

    private String errorDescription;

    public PaymentError(String paymentId, String errorType, String errorDescription) {
        this.paymentId = paymentId;
        this.errorType = errorType;
        this.errorDescription = errorDescription;
    }

    @Override
    public String toString() {
        return "PaymentError{" +
                "paymentId='" + paymentId + '\'' +
                ", errorType='" + errorType + '\'' +
                ", errorDescription='" + errorDescription + '\'' +
                '}';
    }
}
