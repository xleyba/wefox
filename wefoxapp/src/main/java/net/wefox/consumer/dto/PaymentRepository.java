package net.wefox.consumer.dto;


import net.wefox.consumer.payment.domain.Payment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
 * Payment repository definition.
 */
@Repository
public interface PaymentRepository  extends CrudRepository<Payment, String> {

    List<Payment> findAll();

    Payment save(Payment p);

}