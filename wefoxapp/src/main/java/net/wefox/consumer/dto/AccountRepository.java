package net.wefox.consumer.dto;


import net.wefox.consumer.payment.domain.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
 * Account repository definition
 */
@Repository
public interface AccountRepository  extends CrudRepository<Account, Integer> {

    List<Account> findAll();

    Account save(Account a);

}
